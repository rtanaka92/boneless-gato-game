﻿using UnityEngine;
using System.Collections;

public class RotationScript : MonoBehaviour {
    public Vector3 speedRotation;
    public Vector3 nextSpeedRotation;
    public Vector3 speedRotationMin;
    public Vector3 speedRotationMax;
    public bool speedRotationRandomEnable;
    public float rotationTimerChangeMin;
    public float rotationTimerChangeMax;
    public float currentRotationTimer;
    public float timer;
    public Vector3 speedMovement;
    
    // Use this for initialization
    void Start () {
        nextSpeedRotation = speedRotation;
        

    }
	
	// Update is called once per frame
	void Update () {
        ChangeSpeed();


        transform.Rotate(speedRotation*Time.deltaTime);

	}

    void ChangeSpeed()
    {
        if(speedRotationRandomEnable)
        {
            timer += Time.deltaTime;
            if(timer>=currentRotationTimer)
            {
                currentRotationTimer = Random.Range(rotationTimerChangeMin, rotationTimerChangeMax);
                nextSpeedRotation.x = Random.Range(speedRotationMin.x, speedRotationMax.x) * ((Random.value > 0.5) ? 1 : -1);
                nextSpeedRotation.y = Random.Range(speedRotationMin.y, speedRotationMax.y) * ((Random.value > 0.5) ? 1 : -1);
                nextSpeedRotation.z = Random.Range(speedRotationMin.z, speedRotationMax.z) * ((Random.value > 0.5) ? 1 : -1);
                timer = 0;
            }
            if(Input.acceleration.x<0)
            {
                if(nextSpeedRotation.y<0)
                {
                    nextSpeedRotation.y *= -1;
                    timer = 0;
                }
            }
            else if (Input.acceleration.x > 0)
            {
                if (nextSpeedRotation.y > 0)
                {
                    nextSpeedRotation.y *= -1;
                    timer = 0;
                }
            }
            speedRotation += ((nextSpeedRotation-speedRotation)*Time.deltaTime)/currentRotationTimer;
            //speedRotation = nextSpeedRotation;
            
        }
    }
}

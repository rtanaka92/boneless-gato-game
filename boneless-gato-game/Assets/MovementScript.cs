﻿using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Input.gyro.enabled = true;
    }
	
	// Update is called once per frame
	void Update () {

        Vector3 position = transform.localPosition;

        position.x -= Input.acceleration.x * (Time.deltaTime * 3);

        if (position.x >= 2.5f)
        {
            position.x = 2.5f;

        }
        else if (position.x <= -2.5f)
        {
            position.x = -2.5f;

        }
        transform.localPosition = position;
    }
}
